<?php
/**
 * Plugin Name: dkzr Getwid enhancements
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/getwid-enhancements
 * Description: Add anchor option to blocks currently not supporting it
 * Author: Joost de Keijzer
 * Version: 1.0
 */

add_action( 'wp_default_scripts', function( $scripts ) {
  $plugin_dir = __DIR__;

  $script_path       = 'build/index.js';
  $script_asset_path = 'build/index.asset.php';

  if ( file_exists( "{$plugin_dir}/{$script_asset_path}" ) ) {
    $script_asset = require( "{$plugin_dir}/{$script_asset_path}" );
  } else {
    if( ! function_exists('get_plugin_data') ){
      require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }
    $script_asset= [ 'dependencies' => [], 'version' => get_plugin_data( __FILE__ )['Version'] . '-' .filemtime( "{$plugin_dir}/{$script_path}" ) ];
  }

  $script_url = plugin_dir_url( __FILE__ ) . "{$script_path}";
  $scripts->add( 'dkzr-getwid-enhancements-script', $script_url, $script_asset['dependencies'], $script_asset['version'] );

  if ( isset( $scripts->registered['editor'] ) ) {
    array_push( $scripts->registered['editor']->deps, 'dkzr-getwid-enhancements-script' );
  }
}, 100, 1 );
