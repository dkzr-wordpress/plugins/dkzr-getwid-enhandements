import { addFilter } from '@wordpress/hooks';
import { createElement, renderToString } from '@wordpress/element';

let modified = [];

function addAnchorSupport( settings ) {
  if ( settings.name.substring(0, 7) !== 'getwid/' || (settings.supports && settings.supports.anchor) ) {
    return settings;
  }

  modified.push(settings.name);

  return {
    ...settings,
    supports: {
      ...settings.supports,
      anchor: true,
    },
  };
}

//function addSaveProps(props, blockType, attributes) {
//console.log('addSaveProps', props, blockType, attributes);
//  return props;
//}

function saveElement(element, blockType, attributes) {
  if (modified.includes(blockType.name) && element.props.id) {
    const parser = new DOMParser(),
          html = parser.parseFromString( renderToString( element ), 'text/html' );
    html.body.firstElementChild.setAttribute( 'id', element.props.id );

    // create an empty (wrapper) element seems to be allowed?
    return createElement( '', { dangerouslySetInnerHTML: { __html: html.body.innerHTML } } );
  }
  return element;
}

//add filters

addFilter(
  'blocks.registerBlockType',
  'dkzr/getwid/addAnchorSupport',
  addAnchorSupport,
);

//addFilter(
//  'blocks.getSaveContent.extraProps',
//  'dkzr/getwid/addSaveProps',
//  addSaveProps,
//);

addFilter(
  'blocks.getSaveElement',
  'dkzr/getwid/saveElement',
  saveElement,
);
